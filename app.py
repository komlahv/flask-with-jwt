from flask import Flask
from flask_cors import CORS
from .user.models import db
from .user.serializers import ma
from . import todo, user
import os


app = Flask(__name__)
cors = CORS(app)
os.environ['SECRET_KEY'] = 'thisissecret'
# Database
# app.config['SQLALCHEMY_DATABASE_URI'] = sqlite:////mnt/c/Users/vic/Documents/api_example/todo.db'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://username:password@url/db_name'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://username:password@localhost/db_name'
# Use this method in production
# app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost/test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
ma.init_app(app)

app.register_blueprint(user.views.blueprint)
app.register_blueprint(todo.views.blueprint)


if __name__ == '__main__':
    app.run(debug=True)

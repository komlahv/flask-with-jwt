from flask_marshmallow import Marshmallow
# Init ma
ma = Marshmallow()


class UserSchema(ma.Schema):
    class Meta:
        strict = True
        fields = ('public_id', 'name', 'password', 'admin')


# Init schema
user_schema = UserSchema()
users_schema = UserSchema(many=True)

from flask_marshmallow import Marshmallow
# Init ma
ma = Marshmallow()


class TodoSchema(ma.Schema):
    class Meta:
        strict = True
        fields = ('id', 'text', 'complete')


# Init schema
todo_schema = TodoSchema()
todos_schema = TodoSchema(many=True)

from flask import request, jsonify
import jwt
from functools import wraps
from .user.models import User
import os


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
            # token = request.args.get(token) eg.http://127.0.0.1/route?token=ajehiuq3nwzej

        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            data = jwt.decode(token, os.environ['SECRET_KEY'])
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except Exception as e:
            print(e)
            return jsonify({'message': str(e)}), 401

        return f(current_user, *args, **kwargs)

    return decorated
